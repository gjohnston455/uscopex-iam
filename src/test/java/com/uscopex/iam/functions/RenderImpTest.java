package com.uscopex.iam.functions;

import static org.junit.jupiter.api.Assertions.*;

import org.springframework.mock.web.MockMultipartFile;

import ij.ImagePlus;

import org.junit.jupiter.api.Test;

class RenderImpTest {

	@Test
	void testRender() {
		RenderImp renderImp = new RenderImp();
        MockMultipartFile multipartFile = new MockMultipartFile(
        		"data", "filename.txt",
				"text/plain", "some xml".getBytes());
		ImagePlus imp = renderImp.render(multipartFile);
		assertEquals(ImagePlus.class,imp.getClass());
	}
}
