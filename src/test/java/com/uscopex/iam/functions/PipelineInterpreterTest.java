package com.uscopex.iam.functions;


import java.io.File;
import org.junit.jupiter.api.Test;

class PipelineInterpreterTest {
	
	
	@Test
	void testDeleteTempPipeline() {
		File file = new File("test_file");
		PipelineInterpreter.deleteTempPipeline("test_file");
		assert !file.exists();
	}

}
