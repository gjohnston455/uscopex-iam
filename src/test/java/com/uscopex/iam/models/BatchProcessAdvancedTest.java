package com.uscopex.iam.models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ij.ImagePlus;

class BatchProcessAdvancedTest {

	@Test
	void testBatchProcessAdvancedImagePlusStringStringPipelinePropertiesStringIntIntROIScale() {
		assertNotNull(new BatchProcessAdvanced(
				new ImagePlus(), "photoName", "processName", new PipelineProperties(), "description", 1, 1,new ROI(),new Scale()));
	}

}
