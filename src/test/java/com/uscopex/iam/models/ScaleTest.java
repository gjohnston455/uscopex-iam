package com.uscopex.iam.models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ScaleTest {

	@Test
	void testScale() {
		assertNotNull(new Scale(1,"test",1));
	}
	
	@Test
	void testDefaultScale() {
		assertNotNull(new Scale());
	}	
}
