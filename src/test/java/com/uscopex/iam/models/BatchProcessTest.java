package com.uscopex.iam.models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ij.ImagePlus;

class BatchProcessTest {

	@Test
	void testBatchProcessImagePlusStringStringPipelinePropertiesStringIntInt() {
		assertNotNull(new BatchProcess(
				new ImagePlus(), "photoName", "processName", new PipelineProperties(), "description", 1, 1));
	}

}