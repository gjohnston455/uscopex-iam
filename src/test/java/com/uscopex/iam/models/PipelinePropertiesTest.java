package com.uscopex.iam.models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PipelinePropertiesTest {

	@Test
	void testPipelinePropertiesStringStringIntBooleanBooleanBooleanBoolean() {
		assertNotNull(new PipelineProperties(
				"name", "1.0.0", 1, true, true,true,true));
	}


	@Test
	void testGetPath() {
		PipelineProperties pipelineProperties = new PipelineProperties();
		pipelineProperties.setName("name");
		pipelineProperties.setVersionNumber("1.0.0");
		assertEquals("name_1.0.0.bsh", pipelineProperties.getPath());
	}


}
