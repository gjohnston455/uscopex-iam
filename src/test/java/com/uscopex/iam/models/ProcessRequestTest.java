package com.uscopex.iam.models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ij.ImagePlus;

class ProcessRequestTest {

	@Test
	void testProcessRequestImagePlusStringStringPipelinePropertiesStringInt() {
		assertNotNull(new ProcessRequest(
				new ImagePlus().createImagePlus(), "Photo Name", "Process Name", new PipelineProperties(), "desc", 1));
	}

	@Test
	void testProcessRequest() {
	   assertNotNull(new ProcessRequest());
	}


}
