package com.uscopex.iam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uscopex.iam.functions.IamResultSet;
import com.uscopex.iam.functions.repository.IamRepository;

@Service
public class IamService {

	@Autowired
	private IamRepository iamRepository;

	public IamService() {
	}
	public IamService(IamRepository iamRepository) {
		super();
		this.iamRepository = iamRepository;
	}
	public void insert(IamResultSet iam) {
		iamRepository.save(iam);
	}
}
