package com.uscopex.iam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.uscopex.iam.functions.DBResultPath;
import com.uscopex.iam.functions.repository.IamResultsRepository;
@Service
public class IamResultsService {

	@Autowired
	private IamResultsRepository iamResultsRepository;

	public IamResultsService() {}
	
	public IamResultsService(IamResultsRepository iamResultsRepository) {
	
		this.iamResultsRepository = iamResultsRepository;
	}
	public boolean insert(DBResultPath dBResultPath) {
		if(iamResultsRepository.save(dBResultPath) != null) return true;
		return false;
	}

}
