package com.uscopex.iam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.uscopex.iam.functions.DBImagePath;
import com.uscopex.iam.functions.repository.IamImageRepository;
@Service
public class IamImageService {

	@Autowired
	private IamImageRepository iamImageRepository;

	public IamImageService() {}
	
	public IamImageService(IamImageRepository iamImageRepository) {this.iamImageRepository = iamImageRepository;}
	
	public void insert(DBImagePath dBImagePath) {iamImageRepository.save(dBImagePath);}

}
