package com.uscopex.iam.service;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import com.uscopex.iam.functions.BatchDBImagePath;
import com.uscopex.iam.functions.repository.BatchIamImageRepository;

@Service
public class BatchIamImageService {

	@Autowired
	private BatchIamImageRepository batchIamImageRepository;

	public BatchIamImageService() {
	}

	public BatchIamImageService(BatchIamImageRepository batchIamImageRepository) {

		this.batchIamImageRepository = batchIamImageRepository;
	}

	public void insert(BatchDBImagePath dBImagePath) {
		batchIamImageRepository.save(dBImagePath);

	}

}
