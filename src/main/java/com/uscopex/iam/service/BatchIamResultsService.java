package com.uscopex.iam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.uscopex.iam.functions.BatchDBResultPath;
import com.uscopex.iam.functions.repository.BatchIamResultsRepository;

@Service
public class BatchIamResultsService {

	@Autowired
	private BatchIamResultsRepository batchIamResultsRepository;

	public BatchIamResultsService() {
	}
	
	public BatchIamResultsService(BatchIamResultsRepository batchIamResultsRepository) {
	
		this.batchIamResultsRepository = batchIamResultsRepository;
	}
	public boolean insert(BatchDBResultPath dBResultPath) {
		if(batchIamResultsRepository.save(dBResultPath) != null) return true;
		return false;
	}

}
