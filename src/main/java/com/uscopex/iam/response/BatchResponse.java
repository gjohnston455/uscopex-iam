package com.uscopex.iam.response;

import java.util.ArrayList;

import com.uscopex.iam.models.BatchResult;

public class BatchResponse {
	
	private boolean error;
	private String message;
	private int code;
	private String directory;
	private ArrayList<BatchResult> batchResults;

	
	public BatchResponse(boolean error, String message, int code, String directory, ArrayList<BatchResult> batchResults) {
		this.error = error;
		this.message = message;
		this.code = code;
		this.directory = directory;
		this.batchResults = batchResults;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public ArrayList<BatchResult> getBatchResults() {
		return batchResults;
	}

	public void setBatchResults(ArrayList<BatchResult> batchResults) {
		this.batchResults = batchResults;
	}
}
