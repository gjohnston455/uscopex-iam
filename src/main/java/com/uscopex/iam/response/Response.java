package com.uscopex.iam.response;

public class Response {
	
	private boolean error;
	private String message;
	private int code;
	private String imageName;
	private String resultName;
	
	
	public Response(boolean error, String message, int code, String imageName, String resultName) {
		super();
		this.error = error;
		this.message = message;
		this.code = code;
		this.imageName = imageName;
		this.resultName = resultName;
	}

	public Response() {
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getResultName() {
		return resultName;
	}

	public void setResultName(String resultName) {
		this.resultName = resultName;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return String.format("Response [error=%s, message=%s, code=%s, imageName=%s, resultName=%s]", error, message,
				code, imageName, resultName);
	}
}
