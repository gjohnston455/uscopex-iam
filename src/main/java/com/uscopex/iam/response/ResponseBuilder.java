package com.uscopex.iam.response;

import com.uscopex.iam.models.AdvancedProcessing;
import com.uscopex.iam.models.BatchProcess;
import com.uscopex.iam.models.BatchResult;
import com.uscopex.iam.models.ProcessRequest;

public class ResponseBuilder {
	
	public ResponseBuilder() {}
	
	
	public Response build(ProcessRequest request, boolean resultsProcessed, boolean imageProcessed) {

		Response response = new Response(false, "Process Success", 200, request.getProcessedPhotoName(),
				request.getResultFileName());
		if (request.getPipeline().isProducingResults() & !resultsProcessed) {
			response.setError(true);
			response.setMessage("Results failed to process");
			response.setCode(400);
			response.setResultName(null);
		}
		if (request.getPipeline().isProducingImage() & !imageProcessed) {
			response.setError(true);
			response.setMessage("Image failed to process");
			response.setCode(400);
			response.setImageName(null);
		}
		if (request.getPipeline().isProducingResults() & !resultsProcessed
				&& request.getPipeline().isProducingImage() & !imageProcessed) {
			response.setError(true);
			response.setMessage("Image and results failed to process");
			response.setCode(400);
			response.setImageName(null);
			response.setResultName(null);
		}
		return response;

	}
	
	public Response build(AdvancedProcessing request, boolean resultsProcessed, boolean imageProcessed) {

		Response response = new Response(false, "Process Success", 200, request.getProcessedPhotoName(),
				request.getResultFileName());
		if (request.getPipeline().isProducingResults() & !resultsProcessed) {
			response.setError(true);
			response.setMessage("Results failed to process");
			response.setCode(400);
			response.setResultName(null);
		}
		if (request.getPipeline().isProducingImage() & !imageProcessed) {
			response.setError(true);
			response.setMessage("Image failed to process");
			response.setCode(400);
			response.setImageName(null);
		}
		if (request.getPipeline().isProducingResults() & !resultsProcessed
				&& request.getPipeline().isProducingImage() & !imageProcessed) {
			response.setError(true);
			response.setMessage("Image and results failed to process");
			response.setCode(400);
			response.setImageName(null);
			response.setResultName(null);
		}
		return response;

	}
	public BatchResult build(BatchProcess request,boolean resultsProcessed, boolean imageProcessed) {
		
		
		BatchResult batchResult = new BatchResult(request.getProcessedPhotoName(), request.getResultFileName(),false);
		
		if (request.getPipeline().isProducingResults() & !resultsProcessed) {
			batchResult.setError(true);
			batchResult.setResultPath(null);
			
		}
		if (request.getPipeline().isProducingImage() & !imageProcessed) {
			batchResult.setError(true);
			batchResult.setImagePath(null);
		}
		if (request.getPipeline().isProducingResults() & !resultsProcessed
				&& request.getPipeline().isProducingImage() & !imageProcessed) {
			batchResult.setError(true);
			batchResult.setImagePath(null);
			batchResult.setResultPath(null);
		}
		return batchResult;
		
	}
}
