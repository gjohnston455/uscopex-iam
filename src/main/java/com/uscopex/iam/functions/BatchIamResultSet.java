package com.uscopex.iam.functions;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Batch_Iam_Result_Set")
public class BatchIamResultSet {

    @Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;
	@Column(name="Name")
	private String name;
	@Column(name="Directory")
	private String directory;
	@Column(name="Date")
    private String dateCreated;
	@Column(name="User_id")
	private int userID;
	@Column(name="Pipeline")
	private String pipeline;
	@Column(name="Pipeline_Version")
	private String pipelineVersion;
	@Column(name="Description")
	private String description;
	
	public BatchIamResultSet() {	
	}
	public BatchIamResultSet(String name, String directory, String timestamp, int userID,String pipeline,String pipelineVersion, String description) {
		this.name = name;
		this.directory = directory;
		this.dateCreated = timestamp;
		this.userID = userID;
		this.pipeline = pipeline;
		this.pipelineVersion = pipelineVersion;
		this.description = description;
	}
	public Long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getDirectory() {
		return directory;
	}

	public String getDateCreated() {
		return dateCreated;
	}
	public int getUserID() {
		return userID;
	}
	public String getPipeline() {
		return pipeline;
	} 
	public String getPipelineVersion() {
		return pipelineVersion;
	}
}
	
	



