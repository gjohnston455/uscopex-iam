package com.uscopex.iam.functions;

import java.sql.Timestamp;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uscopex.iam.functions.repository.BatchIamImageRepository;
import com.uscopex.iam.functions.repository.BatchIamResultsRepository;
import com.uscopex.iam.functions.repository.IamImageRepository;
import com.uscopex.iam.functions.repository.IamResultsRepository;
import com.uscopex.iam.functions.repository.IamRepository;
import com.uscopex.iam.models.AdvancedProcessing;
import com.uscopex.iam.models.BatchProcess;
import com.uscopex.iam.models.BatchProcessAdvanced;
import com.uscopex.iam.models.BatchResult;
import com.uscopex.iam.models.ProcessRequest;
import com.uscopex.iam.response.Response;
import com.uscopex.iam.response.ResponseBuilder;
import com.uscopex.iam.service.BatchIamImageService;
import com.uscopex.iam.service.BatchIamResultsService;
import com.uscopex.iam.service.IamImageService;
import com.uscopex.iam.service.IamResultsService;
import com.uscopex.iam.service.IamService;

import bsh.Interpreter;

/**
 * Main service class where all endpoints route to.
 */
@Component
public class ProcessService {

	@Autowired
	private IamRepository iamrepo;
	@Autowired
	private IamResultsRepository iamResultsRepo;
	@Autowired
	private IamImageRepository iamImageRepo;
	@Autowired
	private BatchIamImageRepository batchIamImageRepo;
	@Autowired
	private BatchIamResultsRepository batchIamResultsRepo;

	/**
	 * Accesses basic processing methods basic processing does not use ROI or Scale
	 * objects
	 * 
	 * @param processRequest
	 * @return Response object
	 */
	public Response basicProcessing(ProcessRequest processRequest) {
		boolean imageProcessed = false;
		boolean resultsProcessed = false;
		Interpreter interpreter = new PipelineInterpreter().executePipeline(processRequest);
		IamResultSet dbInsertId = insertSingularProcessRequest(processRequest);

		if (processRequest.getPipeline().isProducingResults()) {
			if (processResults(interpreter, processRequest)) {
				resultsProcessed = true;
				inserResultPathSingularProcess(processRequest.getResultFileName(), dbInsertId);
			}
		}

		if (processRequest.getPipeline().isProducingImage()) {
			if (uploadProcessedImage(interpreter, processRequest)) {
				imageProcessed = true;
				insertImagePathSingularProcess(processRequest.getProcessedPhotoName(), dbInsertId);
			}
		}

		return new ResponseBuilder().build(processRequest, resultsProcessed, imageProcessed);

	}

	/**
	 * Accesses similar processing methods to basic. Additionally provides
	 * interpreter injection for ROI and Scale objects advanced processing makes use
	 * ROI and/or Scale objects
	 * 
	 * @param advancedRequest
	 * @return Response object
	 */
	public Response advancedProcess(AdvancedProcessing advancedRequest) {
		boolean imageProcessed = false;
		boolean resultsProcessed = false;
		Interpreter interpreter = new PipelineInterpreter().executePipelineAdvanced(advancedRequest);
		IamResultSet dbInsertId = insertSingularProcessRequest(advancedRequest);

		if (advancedRequest.getPipeline().isProducingResults()) {
			if (processResults(interpreter, advancedRequest)) {
				resultsProcessed = true;
				inserResultPathSingularProcess(advancedRequest.getResultFileName(), dbInsertId);
			}
		}

		if (advancedRequest.getPipeline().isProducingImage()) {
			if (uploadProcessedImage(interpreter, advancedRequest)) {
				imageProcessed = true;
				insertImagePathSingularProcess(advancedRequest.getProcessedPhotoName(), dbInsertId);
			}
		}

		return new ResponseBuilder().build(advancedRequest, resultsProcessed, imageProcessed);

	}

	/**
	 * Accesses similar processing methods to basic. 
	 * For batch images
	 * 
	 * @param
	 * @return Response object
	 */
	public BatchResult batchProcess(BatchProcess processRequest) {
		boolean imageProcessed = false;
		boolean resultsProcessed = false;
		Interpreter interpreter = new PipelineInterpreter().executePipelineBatch(processRequest);

		if (processRequest.getPipeline().isProducingResults()) {
			if (processResultsBatch(interpreter, processRequest)) {
				resultsProcessed = true;
				insertResultPathBatchProcess(processRequest.getResultFileName(), processRequest.getInsertId());
			}
		}

		if (processRequest.getPipeline().isProducingImage()) {
			if (uploadProcessedImageBatch(interpreter, processRequest)) {
				imageProcessed = true;
				insertImagePathBatchProcess(processRequest.getProcessedPhotoName(), processRequest.getInsertId());
			}
		}

		return new ResponseBuilder().build(processRequest, resultsProcessed, imageProcessed);

	}
	

	/**
	 * Accesses similar processing methods to advanced. Small modifications in how
	 * DB/FTP operations perform advanced processing makes use ROI and/or Scale
	 * objects
	 * 
	 * @param
	 * @return Response object
	 */
	public BatchResult batchProcessAdvanced(BatchProcessAdvanced processRequest) {
		boolean imageProcessed = false;
		boolean resultsProcessed = false;
		Interpreter interpreter = new PipelineInterpreter().executePipelineBatchAdvanced(processRequest);

		if (processRequest.getPipeline().isProducingResults()) {
			if (processResultsBatch(interpreter, processRequest)) {
				resultsProcessed = true;
				insertResultPathBatchProcess(processRequest.getResultFileName(), processRequest.getInsertId());
			}
		}

		if (processRequest.getPipeline().isProducingImage()) {
			if (uploadProcessedImageBatch(interpreter, processRequest)) {
				imageProcessed = true;
				insertImagePathBatchProcess(processRequest.getProcessedPhotoName(), processRequest.getInsertId());
			}
		}

		return new ResponseBuilder().build(processRequest, resultsProcessed, imageProcessed);

	}

	
	
	
	
	
	

	/**
	 * Inserts the process request entity with details of the request to database.
	 * 
	 * @param processRequest
	 * @return iamResultSet: AI PK for inserting as FK in path/result table
	 */
	public IamResultSet insertSingularProcessRequest(ProcessRequest processRequest) {
		IamService iamService = new IamService(iamrepo);
		IamResultSet iamResultSet = new IamResultSet(processRequest.getProcessName(), timeOfRequest(),
				processRequest.getUserID(), processRequest.getPipeline().getVersionId(),
				processRequest.getDescription(), 0);
		iamService.insert(iamResultSet);
		return iamResultSet;
	}

	private boolean inserResultPathSingularProcess(String resultFileName, IamResultSet dbInsertId) {
		return new IamResultsService(iamResultsRepo).insert(new DBResultPath(resultFileName, dbInsertId));
	}

	private void insertImagePathSingularProcess(String imagePath, IamResultSet dbInsertId) {
		new IamImageService(iamImageRepo).insert(new DBImagePath(imagePath, dbInsertId));
	}

	private void insertImagePathBatchProcess(String imagePath, int batchId) {
		new BatchIamImageService(batchIamImageRepo).insert(new BatchDBImagePath(imagePath, batchId));
	}

	private boolean insertResultPathBatchProcess(String resultPath, int insertId) {
		return new BatchIamResultsService(batchIamResultsRepo).insert(new BatchDBResultPath(resultPath, insertId));
	}

	/**
	 * Extracts results from interpreter and uploads to server. For singular
	 * Processes
	 * 
	 * @param i              Interpreter
	 * @param processRequest
	 */
	public boolean processResults(Interpreter i, ProcessRequest processRequest) {
		ResultProcessor resultsProcessor = new ResultProcessor();
		boolean success = resultsProcessor.saveResultsToFile(i, processRequest.getResultFileName(),
				processRequest.getUserID());
		return success;
	}

	/**
	 * Extracts results from interpreter and uploads to server. For batch Processes
	 * 
	 * @param i              Interpreter
	 * @param processRequest
	 */
	public boolean processResultsBatch(Interpreter i, BatchProcess processRequest) {
		ResultProcessor resultsProcessor = new ResultProcessor();
		boolean success = resultsProcessor.saveResultsToFile(i, processRequest.getResultFileName(),
				processRequest.getProcessName(), processRequest.getUserID());
		return success;
	}

	/**
	 * Extracts processed imp from interpreter and uploads to server. Used for
	 * Singular processes
	 * 
	 * @param i
	 * @param processRequest
	 * @return
	 */
	public boolean uploadProcessedImage(Interpreter i, ProcessRequest processRequest) {
		UploadProcessedImage imageUploader = new UploadProcessedImage();
		return imageUploader.uploader(i, processRequest.getImp(), processRequest.getProcessedPhotoName(),
				processRequest.getUserID());
	}

	/**
	 * Extracts processed imp from interpreter and uploads to server. Used for Batch
	 * processes
	 * 
	 * @param i
	 * @param processRequest
	 */
	public boolean uploadProcessedImageBatch(Interpreter i, BatchProcess processRequest) {
		UploadProcessedImage imageUploader = new UploadProcessedImage();
		return imageUploader.uploader(i, processRequest.getImp(), processRequest.getProcessedPhotoName(),
				processRequest.getProcessName(), processRequest.getUserID());
	}

	public String timeOfRequest() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		return timestamp.toString();
	}

}
