package com.uscopex.iam.functions;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.uscopex.iam.models.AdvancedProcessing;
import com.uscopex.iam.models.BatchProcess;
import com.uscopex.iam.models.BatchProcessAdvanced;
import com.uscopex.iam.models.BatchROI;
import com.uscopex.iam.models.BatchResult;
import com.uscopex.iam.models.BatchScale;
import com.uscopex.iam.models.PipelineProperties;
import com.uscopex.iam.models.ProcessRequest;
import com.uscopex.iam.models.ROI;
import com.uscopex.iam.models.Scale;
import com.uscopex.iam.response.BatchResponse;
import com.uscopex.iam.response.Response;

@RestController
public class UserResource {

	private static final Logger logger = LoggerFactory.getLogger(UserResource.class);
	@Autowired
	private ProcessService service;

	@PostMapping("/advancedprocess")
	@ResponseBody
	public Response advancedProcessRest(@RequestPart(name = "img") MultipartFile img,
			@RequestPart(name = "processName") String processName,
			@RequestPart(name = "description") String description, @RequestParam(name = "userId") int userId,
			@RequestPart(name = "pipeline_properties") PipelineProperties pipelineProporties,
			@RequestPart(name = "roi") ROI roi, @RequestPart(name = "scale") Scale scale) {


		AdvancedProcessing processRequest = new AdvancedProcessing(roi, scale);
		processRequest.setImp(new RenderImp().render(img));
		processRequest.setDescription(description);
		processRequest.setPhotoName(img.getOriginalFilename());
		processRequest.setProcessName(processName);
		processRequest.setPipeline(pipelineProporties);
		processRequest.setUserID(userId);

		return service.advancedProcess(processRequest);

	}

	@PostMapping("/simpleanalysis")
	@ResponseBody
	public Response basicProcessingRest(@RequestPart(name = "img") MultipartFile img,
			@RequestPart(name = "processName") String processName,
			@RequestPart(name = "description") String description, @RequestParam(name = "userId") int userId,
			@RequestPart(name = "pipeline_properties") PipelineProperties pipelineProporties) {

		RenderImp renderImp = new RenderImp();
		ProcessRequest processRequest = new ProcessRequest(renderImp.render(img), img.getOriginalFilename(),
				processName, pipelineProporties, description, userId);

		return service.basicProcessing(processRequest);
	}

	@PostMapping("/batchprocess")
	@ResponseBody
	public BatchResponse batchProcessingRest(@RequestPart(name = "img") MultipartFile[] img,
			@RequestPart(name = "processName") String processName,
			@RequestPart(name = "pipeline_properties") PipelineProperties pipelineProporties,
			@RequestParam(name = "insertId") int insertId, @RequestParam(name = "userId") int userId) {

		CountDownLatch countDownLatch = new CountDownLatch(img.length);
		boolean processSuccess = true;
		ArrayList<BatchResult> batchResults = new ArrayList<BatchResult>();
		String pipelinePath = pipelineProporties.getPath();
		String localPipelineName = userId + "_" + pipelinePath;

		new PipelineInterpreter().loadPipeline(pipelinePath, localPipelineName);

		for (int i = 0; i < img.length; i++) {
			int finalI = i;
			new Thread(() -> {
				RenderImp renderImp = new RenderImp();
				BatchProcess batchObject = new BatchProcess(renderImp.render(img[finalI]),
						img[finalI].getOriginalFilename(), processName, pipelineProporties, null, userId, insertId);
				System.out.println("path .. " + batchObject.getLocalPipelineName());
				batchResults.add(service.batchProcess(batchObject));
				countDownLatch.countDown();
			}).start();
		}

		try {
			countDownLatch.await();
			PipelineInterpreter.deleteTempPipeline(localPipelineName);
		} catch (InterruptedException e) {
			logger.error("Thread interruption error -> {}",e);
		}

		for (int i = 0; i < batchResults.size(); i++) {
			if (!batchResults.get(i).isError()) {
				processSuccess = false;
				break;
			}
		}

		if (processSuccess) {
			
			return new BatchResponse(true, "Process failed", 401, processName, batchResults);
		} else {

			return new BatchResponse(false, "Process success", 200, processName, batchResults);
		}

	}
	
	@PostMapping("/advancedbatchprocess")
	@ResponseBody
	public BatchResponse advancedBatchProcessingRest(@RequestPart(name = "img") MultipartFile[] img,
			@RequestPart(name = "processName") String processName,
			@RequestPart(name = "pipeline_properties") PipelineProperties pipelineProporties,
			@RequestParam(name = "insertId") int insertId,
			@RequestParam(name = "userId") int userId,
			@RequestPart(name = "rois[]") BatchROI roi,
			@RequestPart(name = "scale[]") BatchScale scale) {
		

		CountDownLatch countDownLatch = new CountDownLatch(img.length);
		boolean processSuccess = true;
		ArrayList<BatchResult> batchResults = new ArrayList<BatchResult>();
		String pipelinePath = pipelineProporties.getPath();
		String localPipelineName = userId + "_" + pipelinePath;
		
		for(int i=0;i<roi.getRoiArray().length;i++) {
			System.out.println(roi.getRoiArray()[i].toString());
		
		}
		new PipelineInterpreter().loadPipeline(pipelinePath, localPipelineName);

		for (int i = 0; i < img.length; i++) {
			int finalI = i;
			new Thread(() -> {
				RenderImp renderImp = new RenderImp();
				BatchProcessAdvanced batchObject = new BatchProcessAdvanced(renderImp.render(img[finalI]),
						img[finalI].getOriginalFilename(),
						processName, pipelineProporties,
						null, userId, insertId,
						roi.getRoiArray()[finalI],
						scale.getScaleArray()[finalI]);
				batchResults.add(service.batchProcessAdvanced(batchObject));
				countDownLatch.countDown();
			}).start();
		}

		try {
			countDownLatch.await();
			PipelineInterpreter.deleteTempPipeline(localPipelineName);
		} catch (InterruptedException e) {
			logger.error("Thread interruption error -> {}",e);
		}

		for (int i = 0; i < batchResults.size(); i++) {
			if (!batchResults.get(i).isError()) {
				processSuccess = false;
				break;
			}
		}

		if (processSuccess) {
			
			return new BatchResponse(true, "Process failed", 401, processName, batchResults);
		} else {
			return new BatchResponse(false, "Process success", 200, processName, batchResults);
		}
	}
}
