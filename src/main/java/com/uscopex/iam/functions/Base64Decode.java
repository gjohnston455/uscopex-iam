package com.uscopex.iam.functions;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;

import ij.ImagePlus;

/**
 * Deprecated
 */
public class Base64Decode {

	/**
	 * Renders an Image Plus object from the base64 string posted to the resting
	 * point
	 * 
	 * @param photo base64 String posted to resting point
	 * @return Image plus object (imp)
	 */
	public ImagePlus renderImp(String photo) {
		//byte[] decodedImg = Base64.getDecoder().decode(photo.getBytes());
		byte[] decodedImg = Base64.getMimeDecoder().decode(photo.getBytes());
		BufferedImage decodedBufferedimg = null;
		try {
			decodedBufferedimg = ImageIO.read(new ByteArrayInputStream(decodedImg));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ImagePlus("image", decodedBufferedimg);
	}
	
	public ImagePlus renderImp2(byte[] photo) {
		
		BufferedImage decodedBufferedimg = null;
		try {
			decodedBufferedimg = ImageIO.read(new ByteArrayInputStream(photo));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ImagePlus("image", decodedBufferedimg);
	}
}
