package com.uscopex.iam.functions;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="Iam_Result_Image_Path")
public class DBImagePath {

    @Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long ID;

	private String path;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "result_set", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
	private IamResultSet Resut_Set;
	
	
	public DBImagePath() {	
	}
	public DBImagePath(String path, IamResultSet Resut_Set) {
		this.path = path;
		this.Resut_Set=Resut_Set;	
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public IamResultSet getResut_Set() {
		return Resut_Set;
	}
	public void setResut_Set(IamResultSet Resut_Set) {
		this.Resut_Set = Resut_Set;
	}
	
}
	
	



