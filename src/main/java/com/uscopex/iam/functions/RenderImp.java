package com.uscopex.iam.functions;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import ij.ImagePlus;

public class RenderImp {

	private static final Logger logger = LoggerFactory.getLogger(RenderImp.class);

	public ImagePlus render(MultipartFile img) {
		
		BufferedImage decodedImage = null;
		try {
			byte[] bytes = img.getBytes();
			decodedImage = ImageIO.read(new ByteArrayInputStream(bytes));
		} catch (IOException e) {
			logger.error("Error when rendering to imp obj -> {}",e);
		}
		return new ImagePlus("image", decodedImage);
	}

}
