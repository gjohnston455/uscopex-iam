package com.uscopex.iam.functions;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import bsh.EvalError;
import bsh.Interpreter;
import ij.ImagePlus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UploadProcessedImage {

	private static final Logger logger = LoggerFactory.getLogger(UploadProcessedImage.class);
	
	public boolean uploader(Interpreter i, ImagePlus imp, String imageName,int userId) {
		String path = imageName;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImagePlus imp2 = null;
			try {
				imp2 = (ImagePlus) i.get("imp");
			} catch (EvalError e) {
				logger.error("Error evaluating imp from executed pipeline interpreter object -> {}",e);
			}
		BufferedImage bufferedImageResult = imp2.getBufferedImage();
			try {
				ImageIO.write(bufferedImageResult, "jpg", os);
			} catch (IOException e1) {
				logger.error("Error converting imp to buffered img -> {}",e1);
			}
		return saveToServer(new ByteArrayInputStream(os.toByteArray()), path, userId);
	}

	public boolean uploader(Interpreter i, ImagePlus imp, String imageName,String directory,int userId ) {
		String path = imageName;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImagePlus imp2 = null;
			try {
				imp2 = (ImagePlus) i.get("imp");
			} catch (EvalError e) {
				logger.error("Error evaluating imp from executed pipeline interpreter object -> {}",e);
			}
		BufferedImage bufferedImageResult = imp2.getBufferedImage();
			try {
				ImageIO.write(bufferedImageResult, "jpg", os);
			} catch (IOException e1) {
				logger.error("Error converting imp to buffered img -> {}",e1);
			}
		return saveToServer(new ByteArrayInputStream(os.toByteArray()), path,directory,userId);
	}

	public boolean saveToServer(ByteArrayInputStream inputStream, String path,String directory, int userId) {
		boolean saved = false;
		FTP ftp = new FTP();
			try {
				ftp.open();
			} catch (IOException e) {
				logger.error("Failed to open ftp connection. Error is {}",e.getMessage());
			}
			try {
				saved = ftp.putProcessedImageToPath(inputStream, path, directory, userId);
			} catch (IOException e) {
				logger.error("Failed to save to directory. Error is {}",e.getMessage());
			}finally {
				try {
					ftp.close();
				} catch (IOException e) {
					logger.error("Failed to open close ftp connection. Error is {}",e.getMessage());
				}
			}
		return saved;
	}

	public boolean saveToServer(ByteArrayInputStream inputStream, String path, int userId) {
		boolean saved = false;
		FTP ftp = new FTP();
			try {
				ftp.open();
			} catch (IOException e) {
				logger.error("Failed to open ftp connection. Error is {}",e.getMessage());
			}
			try {
			saved = ftp.putProcessedImageToPath(inputStream, path, userId);
			} catch (IOException e) {
				logger.error("Failed to save to directory. Error is {}",e.getMessage());
			}finally {

				try {
					ftp.close();
				} catch (IOException e) {
					logger.error("Failed to open close ftp connection. Error is {}",e.getMessage());
				}
			}
		return saved;
	}
}
