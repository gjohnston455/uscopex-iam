package com.uscopex.iam.functions;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Iam_Result_Set")
public class IamResultSet {

    @Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;
	@Column(name="Name")
	private String name;
	@Column(name="Date")
    private String dateCreated;
	@Column(name="User_id")
	private int userID;
	@Column(name="Pipeline")
	private int pipeline;
	@Column(name="Description")
	private String description;
	@Column(name="Is_Batch")
	private int isBatch;
	
	
	public IamResultSet() {	
	}
	public IamResultSet(String name, String timestamp, int userID,int pipeline, String description,int isBatch) {
		this.name = name;
		this.dateCreated = timestamp;
		this.userID = userID;
		this.pipeline = pipeline;
		this.description = description;
		this.isBatch = isBatch;
	}
	public Long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getDateCreated() {
		return dateCreated;
	}
	public int getUserID() {
		return userID;
	}
	public int getPipeline() {
		return pipeline;
	} 
}
	
	



