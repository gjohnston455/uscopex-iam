package com.uscopex.iam.functions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.uscopex.iam.functions.BatchDBImagePath;

@Repository
public interface BatchIamImageRepository extends JpaRepository<BatchDBImagePath, Long> {

}
