package com.uscopex.iam.functions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.uscopex.iam.functions.DBImagePath;

@Repository
public interface IamImageRepository extends JpaRepository<DBImagePath, Long> {

}
