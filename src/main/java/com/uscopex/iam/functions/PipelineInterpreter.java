package com.uscopex.iam.functions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.uscopex.iam.models.AdvancedProcessing;
import com.uscopex.iam.models.BatchProcess;
import com.uscopex.iam.models.BatchProcessAdvanced;
import com.uscopex.iam.models.ProcessRequest;

import bsh.EvalError;
import bsh.Interpreter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main class for loading pipelines and evaluating the scripts.
 * Methods return interpreter objects which can be used to extract objects post evaluation.
 * Overloaded methods for different processing types.
 */
public class PipelineInterpreter {

	private static final Logger logger = LoggerFactory.getLogger(PipelineInterpreter.class);
		
	public void loadPipeline(String path, String uniqueName) {
		FTP ftp = new FTP();
		try {
			ftp.open();
		} catch (IOException e1) {
			logger.error("Failed to open ftp connection. Error is {}",e1.getMessage());
		}
		try {
			ftp.downloadPipeline(path, uniqueName);
			ftp.close();
		} catch (IOException e) {
			logger.error("Failed to open download pipeline. Error is {}",e.getMessage());
		}
	}
	
	
	public Interpreter executePipeline(ProcessRequest processRequest) {

		loadPipeline(processRequest.getPipeline().getPath(),processRequest.getLocalPipelineName());

		Interpreter interpreter = new Interpreter();

		try {
			interpreter.set("imp", processRequest.getImp());
			interpreter.source(processRequest.getLocalPipelineName());
		} catch (FileNotFoundException e) {
			logger.warn("File not found when sourcing pipeline -> {}",e.getMessage());
		} catch (IOException e) {
			logger.error("In out exception occurred -> {}",e.getMessage());
		} catch (EvalError e) {
			logger.error("A script evaluation error occurred when executing pipeline -> {}",e.getMessage());
		}

		deleteTempPipeline(processRequest.getLocalPipelineName());
		return interpreter;
	}
	
	public Interpreter executePipelineBatch(BatchProcess processRequest) {

		Interpreter interpreter = new Interpreter();

		try {
			interpreter.set("imp", processRequest.getImp());
			interpreter.source(processRequest.getLocalPipelineName());
		} catch (FileNotFoundException e) {
			logger.warn("File not found when sourcing pipeline -> {}",e.getMessage());
		} catch (IOException e) {
			logger.error("In out exception occurred -> {}",e.getMessage());
		} catch (EvalError e) {
			logger.error("A script evaluation error occurred when executing pipeline -> {}",e.getMessage());
		}

		return interpreter;
	}
	
	public Interpreter executePipelineAdvanced(AdvancedProcessing processRequest) {
		
		loadPipeline(processRequest.getPipeline().getPath(),processRequest.getLocalPipelineName());
		Interpreter interpreter = new Interpreter();
		
		try {
			interpreter.set("roi", processRequest.getRoi());
			interpreter.set("scale",processRequest.getScale());
			interpreter.set("imp",processRequest.getImp());
			interpreter.source(processRequest.getLocalPipelineName());
		} catch (FileNotFoundException e) {
			logger.warn("File not found when sourcing pipeline -> {}",e.getMessage());
		} catch (IOException e) {
			logger.error("In out exception occurred -> {}",e.getMessage());
		} catch (EvalError e) {
			logger.error("A script evaluation error occurred when executing pipeline -> {}",e.getMessage());
		}
		
		deleteTempPipeline(processRequest.getLocalPipelineName());
		return interpreter;
	}
	
	public Interpreter executePipelineBatchAdvanced(BatchProcessAdvanced processRequest) {

		Interpreter interpreter = new Interpreter();

		try {
			interpreter.set("roi", processRequest.getRoi());
			interpreter.set("scale",processRequest.getScale());
			interpreter.set("imp", processRequest.getImp());
			interpreter.source(processRequest.getLocalPipelineName());
		} catch (FileNotFoundException e) {
			logger.warn("File not found when sourcing pipeline -> {}",e.getMessage());
		} catch (IOException e) {
			logger.error("In out exception occurred -> {}",e.getMessage());
		} catch (EvalError e) {
			logger.error("A script evaluation error occurred when executing pipeline -> {}",e.getMessage());
		}
		
		return interpreter;
	}
	
	public static void deleteTempPipeline(String path) {
		File file = new File(path);
		file.delete();
	}
}
