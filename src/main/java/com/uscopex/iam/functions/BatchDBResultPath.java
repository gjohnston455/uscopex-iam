package com.uscopex.iam.functions;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "iam_result_path")
public class BatchDBResultPath {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String path;

	private int Result_Set;

	public BatchDBResultPath() {
	}

	public BatchDBResultPath(String path, int Result_Set) {
		this.path = path;
		this.Result_Set = Result_Set;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getResut_Set_Id() {
		return Result_Set;
	}

	public void setResut_Set_Id(int Result_Set) {
		this.Result_Set = Result_Set;
	}

}
