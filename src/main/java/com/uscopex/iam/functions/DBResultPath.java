package com.uscopex.iam.functions;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name = "iam_result_path")
public class DBResultPath {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String path;

	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "Result_Set", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
	private IamResultSet Result_Set;

	public DBResultPath() {
	}

	public DBResultPath(String path, IamResultSet Resut_Set) {
		this.path = path;
		this.Result_Set = Resut_Set;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public IamResultSet getResut_Set() {
		return Result_Set;
	}

	public void setResut_Set(IamResultSet Resut_Set) {
		this.Result_Set = Resut_Set;
	}

}
