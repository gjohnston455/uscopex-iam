package com.uscopex.iam.functions;

import java.io.File;
import java.io.IOException;
import bsh.EvalError;
import bsh.Interpreter;
import ij.measure.ResultsTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResultProcessor {

	private static final Logger logger = LoggerFactory.getLogger(RenderImp.class);

	public  boolean saveResultsToFile(Interpreter interpreter, String resultFileName, int userId) {

		String resultName = resultFileName;
		ResultsTable resultsTable = new ResultsTable();
		boolean saved = false;
		try {
			resultsTable = (ResultsTable) interpreter.get("results");
			resultsTable.saveAs(resultFileName);
			saved = saveToServer(resultName, userId);
		} catch (EvalError e) {
			logger.error("Evaluation error when extracting results table -> {}", e);
		} catch (NullPointerException e) {
			logger.error("Null pointer, no results -> {}", e);
		} catch (IOException e) {
			logger.error("Error when saving file -> {}", e);
		} finally {
			resultsTable.reset();
		}
		return saved;
	}
	
	public  boolean saveResultsToFile(Interpreter interpreter, String resultFileName, String directoryName, int userId) {
	
		String resultName = resultFileName;
		ResultsTable resultsTable = new ResultsTable();

		boolean saved = false;
		try {
			resultsTable = (ResultsTable) interpreter.get("results");
			resultsTable.saveAs(resultFileName);
			saved =saveToServer(resultName,directoryName,userId );
		} catch(EvalError e) {
			logger.error("Evaluation error when extracting results table -> {}",e);
		} catch(NullPointerException e) {
			logger.error("Null pointer, no results -> {}",e);
		}catch(IOException e) {
			logger.error("Error when saving file -> {}",e);
			return false;
		}finally {
			resultsTable.reset();
		}
		return saved;
	}
	public boolean saveToServer(String resultName,int userId) {
		boolean saved = false;
		FTP ftp = new FTP();
		File file = new File(resultName);
		try {
			ftp.open();
			saved = ftp.putResultsToPath(file, resultName,userId);
			file.delete();
		} catch (IOException e) {
			logger.error("Error when saving file in remote directory -> {}",e);
		}
		
		return saved;
	}
	public boolean saveToServer(String resultName, String directoryName,int userId) {
		boolean saved = false;
		FTP ftp = new FTP();
		File file = new File(resultName);
		try {
			ftp.open();
			saved = ftp.putResultsToPath(file, resultName,directoryName,userId);
			file.delete();
		} catch (IOException e) {
			logger.error("Error when saving file in remote directory -> {}",e);
		}
		return saved;
	}
}
