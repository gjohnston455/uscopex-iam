package com.uscopex.iam.functions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class provides methods for connecting to remote file directory
 * If this JAR was to be deployed on AWS this would not be needed as files would be locally stored/sourced.
 */
public class FTP {



	static final String PROCESSED_IMAGES_BASE_URL = "/httpdocs/uscopex_web/iam_processes/processed_images/";
	private static final String PROCESSED_RESULTS_BASE_URL = "/httpdocs/uscopex_web/iam_processes/processed_results/";
	private static final String PIPELINE_DIR = "/httpdocs/uscopex_web/iam_pipeline";
	private String server = "gjohnston32.lampt.eeecs.qub.ac.uk";
	private int port = 21;
	private String user = "gjohnston32";
	private String pass = "0C6Z8kvP7TlgtyMh";
	private FTPClient ftpClient;

	void open() throws IOException {
		ftpClient = new FTPClient();
		ftpClient.connect(server, port);
		int reply = ftpClient.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			ftpClient.disconnect();
			throw new IOException("Exception in connecting to FTP Server");
		}
		ftpClient.login(user, pass);
		ftpClient.enterLocalPassiveMode();
	}

	void close() throws IOException {
		ftpClient.disconnect();
	}

	/*
	 * Singular processes image
	 */
	boolean putProcessedImageToPath(InputStream file, String path, int userId) throws IOException {
		String singleAnalysis = "single_analysis";
		ftpClient.changeWorkingDirectory(PROCESSED_IMAGES_BASE_URL);
		if (!ftpClient.changeWorkingDirectory("_" + userId)) {
			ftpClient.makeDirectory("_" + userId);
			ftpClient.changeWorkingDirectory("_" + userId);
		}
		if (!ftpClient.changeWorkingDirectory(singleAnalysis)) {
			ftpClient.makeDirectory(singleAnalysis);
			ftpClient.changeWorkingDirectory(singleAnalysis);
		}
		ftpClient.setBufferSize(1024000);
		ftpClient.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
		return ftpClient.storeFile(path, file);
	}

	/*
	 * Batch processes image
	 */
	boolean putProcessedImageToPath(InputStream file, String path, String directory, int userId) throws IOException {

		ftpClient.changeWorkingDirectory(PROCESSED_IMAGES_BASE_URL);
		if (!ftpClient.changeWorkingDirectory("_" + userId)) {
			ftpClient.makeDirectory("_" + userId);
			ftpClient.changeWorkingDirectory("_" + userId);
		}
		if (!ftpClient.changeWorkingDirectory("batch_analysis")) {
			ftpClient.makeDirectory("batch_analysis");
			ftpClient.changeWorkingDirectory("batch_analysis");
		}
		if (!ftpClient.changeWorkingDirectory(directory)) {
			ftpClient.makeDirectory(directory);
			ftpClient.changeWorkingDirectory(directory);
		}
		ftpClient.setBufferSize(1024000);
		ftpClient.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
		return ftpClient.storeFile(path, file);
	}

	/*
	 * Singular process results
	 */
	boolean putResultsToPath(File file, String path, int userId) throws IOException {
		String singleAnalysis = "single_analysis";
		ftpClient.changeWorkingDirectory(PROCESSED_RESULTS_BASE_URL);
		if (!ftpClient.changeWorkingDirectory("_" + userId)) {
			ftpClient.makeDirectory("_" + userId);
			ftpClient.changeWorkingDirectory("_" + userId);
		}
		if (!ftpClient.changeWorkingDirectory(singleAnalysis)) {
			ftpClient.makeDirectory(singleAnalysis);
			ftpClient.changeWorkingDirectory(singleAnalysis);
		}
		ftpClient.setBufferSize(1024000);
		ftpClient.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
		return ftpClient.storeFile(path, new FileInputStream(file));
	}

	/*
	 * Batch process results
	 */
	boolean putResultsToPath(File file, String path, String directory, int userId) throws IOException {
		String batchDirectory = "batch_analysis";
		ftpClient.changeWorkingDirectory(PROCESSED_RESULTS_BASE_URL);
		if (!ftpClient.changeWorkingDirectory("_" + userId)) {
			ftpClient.makeDirectory("_" + userId);
			ftpClient.changeWorkingDirectory("_" + userId);
		}
		if (!ftpClient.changeWorkingDirectory(batchDirectory)) {
			ftpClient.makeDirectory(batchDirectory);
			ftpClient.changeWorkingDirectory(batchDirectory);
		}
		if (!ftpClient.changeWorkingDirectory(directory)) {
			ftpClient.makeDirectory(directory);
			ftpClient.changeWorkingDirectory(directory);
		}
		ftpClient.setBufferSize(1024000);
		ftpClient.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
		return ftpClient.storeFile(path, new FileInputStream(file));
	}

	void downloadPipeline(String source, String destination) throws IOException {
		FileOutputStream out = new FileOutputStream(destination);
		ftpClient.changeWorkingDirectory(PIPELINE_DIR);
		ftpClient.retrieveFile(source, out);
		out.close();
	}
}
