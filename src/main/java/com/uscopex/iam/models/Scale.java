package com.uscopex.iam.models;

public class Scale {

	
	private int knownDistance;
	private String unitOfMeasurement;
	private int calculatedDistance;

	public Scale() { }

	public Scale(int knownDistance, String unitOfMeasurement, int calculatedDistance) {
		super();
		this.calculatedDistance = calculatedDistance;
		this.knownDistance = knownDistance;
		this.unitOfMeasurement = unitOfMeasurement;
	}

	public int getKnownDistance() {
		return knownDistance;
	}

	public void setKnownDistance(int knownDistance) {
		this.knownDistance = knownDistance;
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}

	public int getCalculatedDistance() {
		return calculatedDistance;
	}

	public void setCalculatedDistance(int calculatedDistance) {
		this.calculatedDistance = calculatedDistance;
	}

	@Override
	public String toString() {
		return String.format("Scale [knownDistance=%s, unitOfMeasurement=%s, calculatedDistance=%s]", knownDistance,
				unitOfMeasurement, calculatedDistance);
	}
}
