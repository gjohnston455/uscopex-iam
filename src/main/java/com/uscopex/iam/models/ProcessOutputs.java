package com.uscopex.iam.models;

public class ProcessOutputs {
	
	private boolean results;
	private boolean image;

	public ProcessOutputs(boolean results, boolean image) {
		super();
		this.results = results;
		this.image = image;
	}
	public boolean isResults() {
		return results;
	}

	public void setResults(boolean results) {
		this.results = results;
	}

	public boolean isImage() {
		return image;
	}

	public void setImage(boolean image) {
		this.image = image;
	}
}
