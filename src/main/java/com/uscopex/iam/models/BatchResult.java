package com.uscopex.iam.models;

public class BatchResult {
	
	private String imagePath;
	private String resultPath;
	private boolean error;
	public BatchResult(String imagePath, String resultPath, boolean error) {
		super();
		this.imagePath = imagePath;
		this.resultPath = resultPath;
		this.error = error;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getResultPath() {
		return resultPath;
	}
	public void setResultPath(String resultPath) {
		this.resultPath = resultPath;
	}
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}

}
