package com.uscopex.iam.models;

import ij.ImagePlus;

public class ProcessRequest {

	private ImagePlus imp;
	private String photoName;
	private String processName;
	private PipelineProperties pipeline;
	private String description;
	private int userID;

	public ProcessRequest() { }

	public ProcessRequest(ImagePlus imp, String photoName,String processName, PipelineProperties pipeline, String description, int userID) {
		super();
		this.imp = imp;
		this.photoName = photoName;
		this.processName = processName;
		this.pipeline = pipeline;
		this.description = description;
		this.userID = userID;
	}

	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public void setPipeline(PipelineProperties pipeline) {
		this.pipeline = pipeline;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public ImagePlus getImp() {
		return imp;
	}

	public void setImp(ImagePlus imp) {
		this.imp = imp;
	}

	public String getPhotoName() {
		return photoName;
	}

	public int getUserID() {
		return userID;
	}

	public String getDescription() {
		return description;
	}

	public String getProcessName() {
		return processName;
	}

	public PipelineProperties getPipeline() {
		return pipeline;
	}

	public String getProcessedPhotoName() {
		return this.getProcessName()+"_"+this.getPhotoName();
	}

	public String getResultFileName() {
		return this.getProcessName() + "_" + this.getUserID() + ".csv";
	}
	
	/**
	 * A unique name applied to the pipeline for saving.
	 * Avoids any interference with any other loaded instances of the pipelines 
	 * @return
	 */
	public String getLocalPipelineName() {
		return this.getUserID()+"_"+this.getPipeline().getPath();
	}


}
