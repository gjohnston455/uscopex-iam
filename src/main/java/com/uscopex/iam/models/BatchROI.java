package com.uscopex.iam.models;

public class BatchROI {
	
	
	private ROI[] roiArray;
	
	public BatchROI() { }

	public BatchROI(ROI[] roiArray) {
		super();
		this.roiArray = roiArray;
	}

	public ROI[] getRoiArray() {
		return roiArray;
	}

	public void setRoiArray(ROI[] roiArray) {
		this.roiArray = roiArray;
	}
}
