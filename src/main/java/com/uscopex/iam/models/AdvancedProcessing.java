package com.uscopex.iam.models;

public class AdvancedProcessing extends ProcessRequest {
	
	private ROI processRoi;
	private Scale scale;
	
	public AdvancedProcessing(ROI processRoi, Scale scale) {
		this.processRoi = processRoi;
		this.scale = scale;
	}
		
	public ROI getRoi() {return processRoi;}

	public Scale getScale() {return scale;}

	public void setScale(Scale scale) {this.scale = scale;}
}
