package com.uscopex.iam.models;

import ij.ImagePlus;

public class BatchProcess extends ProcessRequest{

    private int insertId;

    public BatchProcess() {}

	public BatchProcess(ImagePlus imp, String photoName, String processName, PipelineProperties pipeline,
			String description, int userID,int insertId) {
		super(imp, photoName, processName, pipeline, description, userID);
		this.insertId = insertId;
	}

	public int getInsertId() {
		return insertId;
	}

	public void setInsertId(int insertId) {
		this.insertId = insertId;
	}

	@Override
	public String toString() {
		return String.format("BatchProcess [insertId=%s, toString()=%s]", insertId, super.toString());
	}
}
