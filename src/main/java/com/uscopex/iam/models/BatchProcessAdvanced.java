package com.uscopex.iam.models;

import ij.ImagePlus;

public class BatchProcessAdvanced extends BatchProcess{
	
	private ROI roi;
	private Scale scale;
	public BatchProcessAdvanced() {
		super();
	}
	public BatchProcessAdvanced(ImagePlus imp, String photoName, String processName, PipelineProperties pipeline,
			String description, int userID, int insertId, ROI roi, Scale scale) {
		super(imp, photoName, processName, pipeline, description, userID, insertId);
		this.roi = roi;
		this.scale = scale;
	}
	public ROI getRoi() {
		return roi;
	}

	public void setRoi(ROI roi) {
		this.roi = roi;
	}

	public Scale getScale() {
		return scale;
	}

	public void setScale(Scale scale) {
		this.scale = scale;
	}
	
	
	
	

}
