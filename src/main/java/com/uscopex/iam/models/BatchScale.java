package com.uscopex.iam.models;

public class BatchScale {

	private Scale[] scaleArray;
	
	public BatchScale() { }

	public BatchScale(Scale[] scaleArray) {
		super();
		this.scaleArray = scaleArray;
	}

	public Scale[] getScaleArray() {
		return scaleArray;
	}

	public void setRoiArray(Scale[] scaleArray) {
		this.scaleArray = scaleArray;
	}
}
